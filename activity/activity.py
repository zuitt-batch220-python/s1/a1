name = "Kimberly Carmel Artienda"
age = 28
occupation = "Software Engineer"
movie = "Jeffrey Dahmer Documentary"
rating = 99.9
print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %")

num1 = 10
num2 = 20
num3 = 30
sum = num1 + num2 + num3
print(sum)

is_less_than = num1 < num3
print(is_less_than)
