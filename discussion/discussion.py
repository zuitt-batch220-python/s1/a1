print("Hello World")

# [Section] Variables
# we don't have to use keywords in python to set our variables
# snake case is the convention in terms of naming the variables in python. it uses underscore(_) in between words and uses lowercasing.
age = 35
middle_initial = "C"
print(age)
print(middle_initial)
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1)
print(name2)
print(name3)
print(name4)

# [Section] Data Types
# string
full_name = "John Doe"
secret_code = 'Pa$$word'
# number
num_of_days = 365  # integer
pi_approx = 3.1416  # float
complex_num = 1 + 5j  # complex
# boolean
is_learning = True
is_difficult = False

print("Hi! My name is " + full_name)
print(f"Hi! My name is {full_name} and my age is {age}")

# Typecasting
# is python's wat of converting one data type into another since it does not have type coercion like in JS
# from int to string
print("My age is " + str(age))

# from string to int
print(age + int("9876"))

# integer is different from float in python
print(age + int(98.87))

# [Section] Operators
print(2 + 10)  # addition
print(2 - 10)  # subtraction
print(2 * 10)  # multiplication
print(2 / 10)  # division
print(2 % 10)  # remainder
print(2 ** 10)  # exponent

# [Section] Assignment Operators
num1 = 3
print(num1)
num1 += 4
print(num1)
num1 -= 4
print(num1)
num1 *= 4
print(num1)
num1 /= 4
print(num1)
num1 %= 4
print(num1)

# [Section] Comparison Operators
# returns boolean value to compare values
print(1 == "1")
print(1 != "1")
print(1 < 1)
print(1 > 1)
print(1 >= 1)

# [Section] Logical Operators
print(True and False)
print(True or False)
print(not False)
